﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageFlap : MonoBehaviour{

	private GameObject page;
	private int Degree = 120;
	private int i = 0;
	// Use this for initialization
	void Start () {
		page = GameObject.FindWithTag("RightPage");
	}
	
	public void PageFlaper(){
		page.transform.Rotate(0, 0, Time.deltaTime);
	}
	// Update is called once per frame
	void Update () {
		if(i > 250 && i  < 340){
			page.transform.Rotate(0, 0, -Time.deltaTime * Degree);
		}
		if(i == 300)
			GameObject.FindWithTag("FirstPage").SetActive(false);
			
		//if(i == 350)
			
		i++;
	}
}
