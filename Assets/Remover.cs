﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Remover : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            var go = GameObject.FindGameObjectsWithTag("mid");
            if (go.Length > 0)
            {
                Destroy(go[0]);
            }
            else {
                SceneManager.LoadScene("Scene");
            }
        }		

	}
}
