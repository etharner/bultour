﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveToScene2 : MonoBehaviour {

    public void NextSceneIs2()
    {
        SceneManager.LoadScene("Scene2");
    }

}
